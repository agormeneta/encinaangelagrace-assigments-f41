﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ormeneta.Models;

namespace ormeneta.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Profile(string FName, string LName, string MName, string age, string Gender, string Address){

            if(Gender == "Male"){
                ViewData["gender"] = "Mr.";
            }else{
                ViewData["gender"] = "Ms.";
            }
            ViewData["age"] = age;
            ViewData["add"] = Address;
            ViewData["name"] = FName + " " +  MName +" "+LName ;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
