﻿using System;

namespace ange{

    class Icalculator{
        public void calculator(){
            Console.WriteLine("Calculator");
            int fnum, snum;
            Console.Write("Enter First Number: ");
            fnum= Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter Second Number: ");
            snum= Convert.ToInt32(Console.ReadLine());

            int Answer;
            Answer= fnum + snum;
            Console.WriteLine("Answer: " +Answer.ToString());
            Console.ReadLine();
        }
    }

    class Iscicalculator{
        public void scientific(){
            Console.WriteLine("Scientific Calculator");
            int number;
            Console.Write("Enter a number: ");
            number= Convert.ToInt32(Console.ReadLine());

            double answer= (number* (Math.PI)) /180;
            Console.WriteLine(Math.Tan(answer));
            Console.ReadLine();
        }
    }

    class Iconversion{
        public void binary(){
            Console.WriteLine("Converting Binary to Decimal");
            int binaryVal, decimalVal= 0, baseVal= 1, remainder;
            Console.Write("Enter Binary Number: ");
            binaryVal= Convert.ToInt32(Console.ReadLine());

            while (binaryVal > 0){
                remainder=  binaryVal % 10;
                decimalVal= decimalVal + remainder* baseVal;
                binaryVal= binaryVal/10;
                baseVal= baseVal*2;
            }
            Console.Write("Decimal: " +decimalVal);
            Console.ReadLine();
        }        
    }

    class Call{
        static void Main(String[] args){
            Icalculator calc = new Icalculator();
            Iscicalculator scical = new Iscicalculator();
            Iconversion convert = new Iconversion();

            calc.calculator();
            scical.scientific();
            convert.binary();
            System.Environment.Exit(1);
        }
    }
}
