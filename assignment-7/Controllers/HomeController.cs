﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using assignment_7.Models;

namespace assignment_7.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserContext _context;

        public HomeController(UserContext context)
        {
            _context = context;
        }
        public IActionResult Clients()
        {
            return View();
        }
        public IActionResult Index()
        {
            return View();
        }


        public IActionResult Privacy()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult>Clients([Bind("id,f_name,m_name,l_name,age,address")] ClientProfileModel clientsinfo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(clientsinfo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Clients));
            }
            return View(clientsinfo);
        }
    }
}
