using System;
using System.ComponentModel.DataAnnotations;

namespace assignment_7.Models
{
    public class ClientProfileModel
    {
        [Key]
        public int id {get;set;}
        [Required]
        [StringLength(50, ErrorMessage="Required Area")]
        public string f_name{get; set;}
        [Required]
        [StringLength(50, ErrorMessage="Required Area")]
        public string m_name{get; set;}
        [Required]
        [StringLength(50, ErrorMessage="Required Area")]
        public string l_name{get; set;}
        [Required]
        [Range (5,100, ErrorMessage = "Required Area")]
        public int age{get; set;} 
        [Required]
        [StringLength(50, ErrorMessage="Required Area")]
        public string address{get; set;}
    }
    
}