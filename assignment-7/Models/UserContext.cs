using Microsoft.EntityFrameworkCore;
namespace assignment_7.Models
{
    public class UserContext : DbContext
    {
        public UserContext(DbContextOptions<UserContext> options) : base(options)

        {

        }
        public DbSet<ClientProfileModel> ClientsProfile {get; set;}
    }
}