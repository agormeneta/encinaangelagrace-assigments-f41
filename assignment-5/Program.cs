﻿using System;

namespace assignment_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Triangle tri = new Triangle();
        
            try
            {
                tri.Sides();
                tri.calTriangle();
            }
            catch(OneNumberIsNull e){
                Console.WriteLine("OneNumberIsNull: {0}", e.Message);
            }catch(AllNumbesAreNull e){
                Console.WriteLine("AllNumbesAreNull: {0}", e.Message);
            }catch(InvalidOperation e){
                Console.WriteLine("InvalidOperation: {0}", e.Message);
            }
            Console.ReadKey();
        }
    }
    public class Triangle
    {
        float SetA = 13;
        float SetB = 31;
        float SetC = 9;

        public void Sides()
        {
            if (SetA == 0 && SetB == 0 && SetC == 0){
                throw (new AllNumbesAreNull("All sides are null"));
            }
            else if (SetA == 0 && SetB == 0 && SetC != 0) {
                throw (new OneNumberIsNull("All sides are null"));
            }
            else if (SetA == 0 && SetB != 0 && SetC == 0){
                throw (new OneNumberIsNull("All sides are null"));
            }
            else if (SetA != 0 && SetB == 0 && SetC == 0){
                throw (new OneNumberIsNull("One number is null"));
            }
            else if (SetA != 0 && SetB != 0 && SetC != 0){
                throw (new OneNumberIsNull("Leave one null value"));
            }
        }
        public void calTriangle(){
            string operation;

            Console.Write("\nChoose Operation: 1(Calculate A), 2(Calculate B), 3(Calculate C) \n");
            operation = Console.ReadLine();
            double Op = Convert.ToDouble(operation);

            switch (Op){
                case 1:
                    if(SetA != 0){
                        throw (new InvalidOperation("A has a Value"));
                    }else{
                        CalculateA();
                    }
                    break;
                case 2:
                    if(SetB != 0){
                        throw (new InvalidOperation("A has a Value"));
                    }else{
                        CalculateB();       
                    }
                    break;
                
                case 3:
                    if(SetC != 0){
                        throw (new InvalidOperation("A has a Value"));
                    }else{
                        CalculateC();
                    }                    
                    break;
            }
        }
        public void CalculateA(){
            float first = (SetB * SetB) - (SetC * SetC);
            float second = (float)Math.Sqrt(first);

            Console.WriteLine("A is {0}",second);
            float p = (second + SetB + SetC) / 2;

            float x = p * (p-second)*(p-SetB)*(p-SetC);
            Console.Write(Math.Sqrt(x));
            float thetaA = (float)Math.Atan(second/SetB) * (float)(180 / Math.PI);
            float thetaB = (float)Math.Acos(second/SetC) * (float)(180 / Math.PI);
            float thetaC = (float)Math.Asin(SetB/SetC) * (float)(180 / Math.PI);

            Console.WriteLine("\nAngles are {0}",thetaA + " "+ thetaB + " "+ thetaC+"\n");                       
        }
        public void CalculateB(){
            float b = (SetA * SetA) - (SetC * SetC);
            float bb = (float)Math.Sqrt(b);

            Console.WriteLine("B is {0}",bb);
            float p = (SetA + bb + SetC) / 2;

            float pA = p - bb;
            float pB = p - SetB;
            float pC = p - SetC;
            float x = p * pA * pB * pC;
            Console.Write(Math.Sqrt(x));
            float thetaA = (float)Math.Atan(SetA/bb) * (float)(180 / Math.PI);
            float thetaB = (float)Math.Acos(SetA/SetC) * (float)(180 / Math.PI);
            float thetaC = (float)Math.Asin(bb/SetC) * (float)(180 / Math.PI);

            Console.WriteLine("\nAngles are {0}",thetaA + " "+ thetaB + " "+ thetaC+"\n");                        
        }
        public void CalculateC(){
            float c = (SetA * SetA) + (SetB * SetB);
            float cc = (float)Math.Sqrt(c);

            Console.WriteLine("C is {0}",cc);
            float p = (SetA + SetB + cc) / 2;

            float x = p *(p-SetA)*(p-SetB)*(p-cc);
            Console.Write(Math.Sqrt(x));

            float thetaA = (float)Math.Atan(SetA/SetB) * (float)(180 / Math.PI);
            float thetaB = (float)Math.Acos(SetA/cc) * (float)(180 / Math.PI);
            float thetaC = (float)Math.Asin(SetB/cc) * (float)(180 / Math.PI);

            Console.WriteLine("\nAngles are {0}",thetaA + " "+ thetaB + " "+ thetaC+"\n");
        }



    }
    
    public class OneNumberIsNull : Exception{
        public OneNumberIsNull(string message) : base(message){
        }
    }

    public class AllNumbesAreNull : Exception
    {
        public AllNumbesAreNull(string message) : base(message){
        }
    }

    public class InvalidOperation : Exception{
        public InvalidOperation(string message) : base(message){
        }
    }
}

