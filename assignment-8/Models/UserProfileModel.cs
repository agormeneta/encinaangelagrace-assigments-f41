﻿using System.ComponentModel.DataAnnotations;

namespace ass7.Models
{
    public class UserProfileModel
    {
        [Key]
        public int id { get; set; }


        [Required(ErrorMessage = "Name length can't be more than 8.")]
        public string first_name { get; set; }

        [StringLength(8, ErrorMessage = "Name length can't be more than 8.")]
        public string middle_name { get; set; }


        [StringLength(50)]
        [Required]
        public string last_name { get; set; }

        [Range(5,50, ErrorMessage = "Please Enter Valid Age")]
        [Required]
        public int Age { get; set; }

        [StringLength(50)]
        [Required]
        public string gender{get; set;}

        [StringLength(50)]
        [Required]
        public string address{get; set;}

         [Required]  
        [EmailAddress]  
        public string Email { get; set; }  
  
        [Required]  
        [DataType(DataType.Password)]  
        public string Password { get; set; }  
  
        [Required]  
        [Display(Name ="Confirm Password")]  
        [DataType(DataType.Password)]  
        [Compare("Password",ErrorMessage ="Your password and confirm password do not match")]  
        public string ConfirmPassword { get; set; }  
    }
}
