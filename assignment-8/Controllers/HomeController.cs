﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using ass7.Models;
using Microsoft.AspNetCore.Http;

namespace ass7.Controllers
{
    public class HomeController : Controller
    {
        const string SessionID = "";  
        const string SessionName = "";  
        UserContext db = new UserContext();

        public HomeController(UserContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register([Bind("id, first_name, middle_name, last_name, gender, address, Age, Email, Password, ConfirmPassword")] UserProfileModel userProfile)
        {
            if (ModelState.IsValid)
            {
                db.Add(userProfile);
                await db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            } 
            else
            {
                ModelState.AddModelError("", "Some Error Occured");
            }
            return View(userProfile);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserProfileModel UserProfile)
        {   
            var _admin = db.UserProfile.Where(s => s.Email == UserProfile.Email);
            if(_admin.Any()){
                if(_admin.Where(s => s.Password == UserProfile.Password).Any()){

                    return Json(new { status = true, message = "Successfully Logged in"});
                }
                else
                {
                    return Json(new { status = false, message = "Invalid Password!"});
                }   
            }
            else
            {
                return Json(new { status = false, message = "Invalid Email!"});
            }

        }
        public IActionResult UserProfile()
        {
            ViewBag.data = HttpContext.Session.GetString("SessionID"); 
            return View();
        }

    }
}
