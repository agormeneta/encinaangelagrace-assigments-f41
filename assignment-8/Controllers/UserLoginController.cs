using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using ass7.Models;

namespace ass7.Controllers
{
    public class UserLoginController : Controller
    {
        private UserContext db = new UserContext();

        public IActionResult Register()
        {
            return View();
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Register(UserProfileModel userProfile)
        {
            if (ModelState.IsValid)
            {
                db.UserProfile.Add(userProfile);
                db.SaveChanges();
                return RedirectToAction("Profile");
            }
            else
            {
                ModelState.AddModelError("", "Some Error Occured");
            }
            return View(userProfile);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(UserProfileModel userProfile)
        {
            /*if (ModelState.IsValid)
            {
                var obj = .UserProfile.Where(u => u.Email.Equals(userProfile.Email) && u.Password.Equals(userProfile.Password)).FirstOrDefault();
                if (obj != null)
                {
                    return RedirectToAction("LoggedIn");

                }
            } */
            return View(userProfile);
        }
        public IActionResult LoggedIn()
        {
            return View();
        }

    }
}
