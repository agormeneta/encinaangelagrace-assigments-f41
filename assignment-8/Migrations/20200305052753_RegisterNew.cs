﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ass7.Migrations
{
    public partial class RegisterNew : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ConfirmPassword",
                table: "UserProfile",
                nullable: false);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "UserProfile",
                nullable: false);

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "UserProfile",
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConfirmPassword",
                table: "UserProfile");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "UserProfile");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "UserProfile");
        }
    }
}
